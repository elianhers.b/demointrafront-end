import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from './global';

@Injectable({
  providedIn: 'root'
})
export class ListasService {
  public url: string;
  public errorHttp: boolean;
  public pais: any;

  constructor( 
    public _http : HttpClient 
  
  ){ 
      this.url = GLOBAL.url;
  }

  getPais(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'getpais');    
  }

  getDescripcionTrabajo(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'getdescripciontrabajo');    
  }

  getGenero(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'getgenero');
  }

  getRoles(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'getroles');
  }

  getProfesion(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'getprofesion');
  }

  getNacionalidad(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'getnacionalidad');
  }

  getConocimientos(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'getconocimientos');
  }

  getTipoEstudio(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'gettipoestudio');
  }

  getTipoRequerimiento(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'gettiporequerimiento');
  }

  getAreaTrabajo(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'getareatrabajo');
  }

  getCargos(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'getcargos');
  }

  getIdiomas(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'getidiomas');
  }

  getStatus(): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencode');
    return this._http.get(this.url+'getstatus');
  }


}
