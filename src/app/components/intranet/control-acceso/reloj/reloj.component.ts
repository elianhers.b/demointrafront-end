import { Component, OnInit } from '@angular/core';
import { AsistenciasService } from '../../../../services/intranet/asistencias.service';
@Component({
  selector: 'app-reloj',
  templateUrl: './reloj.component.html',
  styleUrls: ['./reloj.component.css']
})
export class RelojComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  	(function () {
	var reloj = function() {
		var hora = new Date(),
			horas = hora.getHours(),
			ampm,
			minutos = hora.getMinutes(),
			mes = (hora.getMonth()+1),
			ms: string,
			min: string,
			hr: string;

	var Vhoras = document.getElementById('horas'),
		Vminutos = document.getElementById('minutos'),
		Vampm = document.getElementById('ampm'),
		fecha = document.getElementById('fecha');

	if(horas >= 12){
		horas = horas - 12;
		ampm = 'P.M';
	} else {
		ampm = 'A.M';
	};

	if(horas == 0){
		horas = 12;
	};

	if(minutos < 10){
		min = '0'+minutos;
	}else{
		min = ''+minutos;
	};

	if(horas < 10){
		hr = '0'+horas;
	}else{
		hr = ''+horas;
	};
	
	if(Vhoras != null){
		Vhoras.textContent = hr;
		Vminutos.textContent = min;
		Vampm.textContent = ampm;
	}


	if(mes<10){
		ms = '0'+mes;
	}else{
		ms = ''+mes;
	}
	
	if(fecha != null){
		fecha.textContent = hora.getDate() + "-" + ms + "-" + hora.getFullYear();
		};
	}

	reloj();
	var intervalo = setInterval(reloj, 1000);
	


}());
  }

}
